from tqdm import tqdm

import torch 
import numpy as np

from model import DimaBert

def put_on_device(input, device):
    if isinstance(input, torch.Tensor):
        return input.to(device)
    else:
        return [el.to(device) for el in input]

class InferenceModel(object):
    def __init__(self, nn_model, device):
        self.nn_model = nn_model
        self.device = device

        self.nn_model.eval()

    def __call__(self, dataloader):
        result = []
        with torch.no_grad():
            for x in tqdm(dataloader):
                x = put_on_device(x, self.device)
                prediction = self.nn_model.forward(*x)
                #prediction = prediction.argmax(dim=-1)
                result.append(prediction.detach().cpu().numpy())

        return np.concatenate(result, axis=0)

