import torch

def get_dima_optimizer(dima_model, clf_lr, base_lr):
    params = list(dima_model.named_parameters())

    def is_backbone(n):
        return "bert" in n

    optimizer_grouped_parameters = [
        {"params": [p for n, p in params if is_backbone(n)], "lr": base_lr},
        {"params": [p for n, p in params if not is_backbone(n)], "lr": clf_lr},
    ]

    dima_optimizer = torch.optim.AdamW(
        optimizer_grouped_parameters, lr=base_lr, weight_decay=0
    )

    return dima_optimizer
    