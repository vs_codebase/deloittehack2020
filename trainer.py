import sys
import numpy as np
import torch
from tqdm import tqdm as tqdm
from meter import AverageValueMeter

def put_on_device(input, device):
    if isinstance(input, torch.Tensor):
        return input.to(device)
    else:
        return [el.to(device) for el in input]


class Epoch:

    def __init__(self, model, loss, stage_name,
                 scalar_freq_logging, writer,
                 metrics=None,
                 device='cpu', verbose=True):

        self.model = model
        self.loss = loss
        self.metrics = metrics
        self.stage_name = stage_name
        self.verbose = verbose
        self.device = device
        self.scalar_freq_logging = scalar_freq_logging
        self.writer = writer

        self._to_device()

    def _to_device(self):
        self.model.to(self.device)
        self.loss.to(self.device)
        if self.metrics is not None:
            self.metrics = self.metrics.to(self.device)

    def _format_logs(self, logs):
        str_logs = ['{} - {:.4}'.format(k, v) for k, v in logs.items()]
        s = ', '.join(str_logs)
        return s

    def batch_update(self, x, y, turn_off_augmix_mixup=False):
        raise NotImplementedError

    def on_epoch_start(self):
        pass
    
    def add_summary_scalar(self, item, iteration, add_anyway=False):
        if add_anyway or iteration % self.scalar_freq_logging == 0:
            for k in item.keys():
                self.writer.add_scalar(self.stage_name+'/'+k, 
                                       item[k],
                                       iteration)
                
    def update_dict(self, dict_to_update, updating_dict):
        for k in dict_to_update.keys():
            dict_to_update[k] += updating_dict[k]
            
        return dict_to_update

    def run(self, dataloader, iteration, turn_off_augmix_mixup=False):

        self.on_epoch_start()

        logs = {}
        loss_meters = {l_n: AverageValueMeter() for l_n in self.loss.__names__}

        with tqdm(dataloader, desc=self.stage_name, file=sys.stdout, disable=not (self.verbose)) as iterator:
            for x, y in iterator:
                x, y = put_on_device(x, self.device), put_on_device(y, self.device)
                batch_len = len(x)
                
                loss_dict, y_pred = self.batch_update(x, y, iteration)

                # update loss logs
                for key in loss_meters.keys():
                    loss_meters[key].add(loss_dict[key], batch_len)
                    
                loss_logs = {k: v.mean for k, v in loss_meters.items()}
                logs.update(loss_logs)
                
                if (self.writer is not None) and self.stage_name == 'train':
                    self.add_summary_scalar(loss_logs, iteration)

                # update metrics logs
                if self.stage_name == 'valid' and self.metrics is not None:
                    metrics_logs = self.metrics(y_pred, y)
                    logs.update(metrics_logs)

                # verbosity logs
                if self.verbose:
                    s = self._format_logs(logs)
                    iterator.set_postfix_str(s)

                if (
                    self.stage_name == 'train' and 
                    hasattr(self, "scheduler") and 
                    self.scheduler is not None
                    ):
                        self.writer.add_scalar(
                                'model/BaseLR', 
                                self.optimizer.param_groups[0]['lr'],
                                iteration
                                )
                        self.writer.add_scalar(
                                'model/CLFLR', 
                                self.optimizer.param_groups[1]['lr'],
                                iteration
                                            )
                    
                if self.stage_name == 'train':
                    iteration += 1
                
        # update val logs
        if (self.writer is not None) and self.stage_name == 'valid':
            self.add_summary_scalar(loss_logs, iteration, add_anyway=True)
            if self.metrics is not None:
                self.add_summary_scalar(metrics_logs, iteration, add_anyway=True)

        return logs, iteration


class TrainEpoch(Epoch):

    def __init__(self, model, loss, optimizer,
                 scalar_freq_logging, writer,
                 gradient_accumulation=1,
                 scheduler=None,
                 metrics=None, 
                 device='cpu', verbose=True):
        super().__init__(
            model=model,
            loss=loss,
            metrics=metrics,
            stage_name='train',
            device=device,
            verbose=verbose,
            scalar_freq_logging=scalar_freq_logging,
            writer=writer,
        )
        self.gradient_accumulation = gradient_accumulation

        self.scheduler = scheduler
        self.optimizer = optimizer

    def on_epoch_start(self):
        self.model.train()
        if self.metrics is not None:
            self.metrics.reset()

    def batch_update(self, x, y, iteration):
        y_hat = self.model.forward(*x)
        loss, loss_dict = self.loss(y_hat, y)

        loss.backward()
        if (iteration + 1) % self.gradient_accumulation == 0:
            self.optimizer.step()
            self.scheduler.step()
            self.optimizer.zero_grad()
            
        return loss_dict, None


class ValidEpoch(Epoch):

    def __init__(self, model, loss,
                 scalar_freq_logging, writer, 
                 metrics=None,
                 device='cpu', verbose=True):
        super().__init__(
            model=model,
            loss=loss,
            metrics=metrics,
            stage_name='valid',
            device=device,
            verbose=verbose,
            scalar_freq_logging=scalar_freq_logging,
            writer=writer,
        )

    def on_epoch_start(self):
        self.model.eval()
        if self.metrics is not None:
            self.metrics.reset()

    def batch_update(self, x, y, iteration):
        with torch.no_grad():
            prediction = self.model.forward(*x)
            loss, loss_dict = self.loss(prediction, y)
  
        return loss_dict, prediction
