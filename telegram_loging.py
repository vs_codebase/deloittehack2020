import telegram

class TelegramCallback(object):

    def __init__(self, config, name=None):
        super(TelegramCallback, self).__init__()
        self.user_id = config['telegram_id']
        self.bot = telegram.Bot(config['token'])

    def send_message(self, text):
        try:
            self.bot.send_message(chat_id=self.user_id, text=text)
        except Exception as e:
            print('Message did not send. Error: {}.'.format(e))