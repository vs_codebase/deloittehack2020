import argparse
import os
import json

from os import path

import torch
import numpy as np
import pandas as pd

from model import DimaBert
from inference_model import InferenceModel
from loader import BertDataset
from metrics import accuracy_metric

def create_simple_pred_from_exploded(pred, df):
    df['pred'] = [list(el) for el in pred]
    
    reduced_pred = df.groupby('id')['pred'].apply(lambda x: np.array(list(map(np.array,x))).mean(axis=0))
    
    df = df.drop(columns='pred')
    return reduced_pred

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str,
                        help='path to json inference config')
    args = parser.parse_args()

    # Parse configs
    with open(args.config) as f:
        data = f.read()
    config = json.loads(data)

    # Load model
    model = DimaBert.from_pretrained(
        config['bert_model_to_use'],
        num_labels=config['num_labels']
    ).to(config['device'])
    chckp = torch.load(
        config['checkpoint_path'], 
        map_location=config['device']
    )['model']
    model.load_state_dict(chckp)

    model = InferenceModel(
        nn_model=model,
        device=config['device']
    )

    # Load test data
    test_ids = torch.load(config['test_ids_path'])
    test_mask = torch.load(config['test_mask_path'])
    test_segment = torch.load(config['test_segment_path'])

    test_dataset = train_dataset = BertDataset(
        ids=test_ids,
        masks=test_mask,
        segments=test_segment,
    )
    test_dataloader = torch.utils.data.DataLoader(
        test_dataset, 
        batch_size=config['batch_size'], 
        shuffle=False,
        num_workers=os.cpu_count()//2
    )

    # Load val data
    cv_test_idx= np.load(config['cv_test_idx_path'], allow_pickle=True)[config['fold_number']]

    val_ids = torch.load(config['train_ids_path'])
    val_mask = torch.load(config['train_mask_path'])
    val_segment = torch.load(config['train_segment_path'])
    target = torch.load(config['target_path'])[cv_test_idx]
    val_dataset = BertDataset(
        ids=val_ids[cv_test_idx],
        masks=val_mask[cv_test_idx],
        segments=val_segment[cv_test_idx],
    )
    val_dataloader = torch.utils.data.DataLoader(
        val_dataset, 
        batch_size=config['batch_size'], 
        shuffle=False,
        num_workers=os.cpu_count()//2
    )

    # Predict on val data
    val_pred = model(val_dataloader)

    exploded_metric = accuracy_metric(target, val_pred.argmax(-1))

    val_df = pd.read_pickle(config['train_df_path'])
    val_df = val_df.explode('text').reset_index(drop=True).iloc[cv_test_idx]
    val_real_pred = create_simple_pred_from_exploded(val_pred, val_df)

    val_df = val_df.set_index('id')
    val_df['pred'] = val_real_pred.apply(lambda x: x.argmax(-1))

    val_df = val_df.reset_index().drop_duplicates('id')

    real_metric = accuracy_metric(val_df['target'], val_df['pred'])

    # Predict on test data
    test_df = pd.read_pickle(config['test_df_path'])
    test_df = test_df.explode('text').reset_index(drop=True)
    test_pred = model(test_dataloader)
    test_pred = create_simple_pred_from_exploded(test_pred, test_df)

    os.makedirs(path.dirname(config['target_dir']), exist_ok=True)
    test_pred.reset_index().to_pickle(config['target_dir'])

    print('Exploaded metric: {}'.format(exploded_metric))
    print('Real metric: {}'.format(real_metric))

    print('OtBertili')