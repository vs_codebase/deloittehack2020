from copy import deepcopy

import torch
import numpy as np

def torch_isin(array, in_list):
    for idx, el in enumerate(in_list):
        if idx: 
            bool_tensor = bool_tensor + (array == el)
        else:
            bool_tensor = array == el
            
    return bool_tensor

class RandomTokenPerm(object):
    def __init__(self, p=0.5, min_toks=10, max_toks=100):
        self.p = p
        self.min_toks = min_toks
        self.max_toks = max_toks
        
        self.stop_tocks = [0, 101, 102]
        
    def __call__(self, ids, masks, segments, y):
        
        if np.random.binomial(n=1, p=self.p):
            valid_indices = torch.where(
                ~torch_isin(ids, self.stop_tocks)
            )[0]
            
            if len(valid_indices) < self.min_toks + 1:
                return ids, masks, segments, y
            
            indices_to_perm = np.random.randint(
                low=self.min_toks, 
                high=min(len(valid_indices), self.max_toks)
            )
            
            original_indices = np.random.choice(valid_indices, size=indices_to_perm)
            permuted_indices = deepcopy(original_indices)
            np.random.shuffle(permuted_indices)
            ids[original_indices] = ids[permuted_indices]
            
        return ids, masks, segments, y
            