from math import floor, ceil

import torch
import numpy as np

class BertDataset(torch.utils.data.Dataset):
    def __init__(
        self, 
        ids, 
        masks,
        segments=None,
        y=None,
        augmentations=None
    ):
        self.ids = ids
        self.masks = masks
        self.segments = segments
        
        self.y = y

        self.augmentations = augmentations
        
    def __getitem__(self, idx):
        ids = self.ids[idx]
        masks = self.masks[idx]
        segments = self.segments[idx] if self.segments is not None else None

        y = self.y[idx] if self.y is not None else None

        if self.augmentations is not None:
            for aug in self.augmentations:
                ids, masks, segments, y = aug(ids, masks, segments, y)

        if segments is not None:
            x = (ids, masks, segments)
        else:
            x = (ids, masks)

        if y is None:
            return x 
        else:
            return x, y 

    def __len__(self):
        return len(self.ids)
