import numpy as np


class Meter(object):
    '''Meters provide a way to keep track of important statistics in an online manner.
    This class is abstract, but provides a standard interface for all meters to follow.
    '''

    def reset(self):
        '''Resets the meter to default settings.'''
        pass

    def add(self, value):
        '''Log a new value to the meter
        Args:
            value: Next restult to include.
        '''
        pass

    def value(self):
        '''Get the value of the meter in the current state.'''
        pass


class AverageValueMeter(Meter):
    def __init__(self):
        super(AverageValueMeter, self).__init__()        
        self.reset()

    def add(self, value, batch_len):
        self.n += batch_len
        self.sum += value * batch_len
        
        self.mean = self.sum / self.n 

    def value(self):
        return self.mean

    def reset(self):
        self.n = 0
        self.sum = 0
        self.mean = np.nan
