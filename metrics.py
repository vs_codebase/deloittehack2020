import numpy as np
import torch
import torch.nn as nn

from scipy.stats import spearmanr
from sklearn.metrics import accuracy_score

def accuracy_metric(y_true, y_pred, return_scores=False):
    weights = np.ones_like(y_true)

    weights[y_true == 2] = 2

    return accuracy_score(
        y_true,
        y_pred,
        sample_weight=weights
    )

class AccuracyMetric(nn.Module):
    def __init__(self):
        super().__init__()
        self.y_true_acum = None
        self.y_pred_acum = None

    def update(self, y_pred, y_true):
        # y_pred - [B, C]
        # y_true - [B, C]
        y_pred = y_pred.argmax(-1)

        y_true = y_true.detach().cpu().numpy()
        y_pred = y_pred.detach().cpu().numpy()
        
        if self.y_true_acum is None:
            self.y_true_acum = y_true
        else:
            self.y_true_acum = np.concatenate([
                self.y_true_acum,
                y_true
            ], axis=0)

        if self.y_pred_acum is None:
            self.y_pred_acum = y_pred
        else:
            self.y_pred_acum = np.concatenate([
                self.y_pred_acum,
                y_pred
            ], axis=0)
        
    def compute(self):        
        return accuracy_metric(self.y_true_acum, self.y_pred_acum)

    def forward(self, y_pred, y_true):
        self.update(y_pred, y_true)
        metric_value = self.compute()
        metric_dict = {'metric':metric_value}
        return metric_dict

    def reset(self):
        self.y_pred_acum = None
        self.y_true_acum = None
