import argparse

from os import path

import pandas as pd
import numpy as np

from sklearn.model_selection import StratifiedKFold, GroupKFold


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--df_path', type=str,
                        help='path to dataframe')
    parser.add_argument('--stratification_strategy', type=str,
                        help='can be groups_stratify or target_stratify')
    args = parser.parse_args()

    train = pd.read_pickle(args.df_path)

    # Create CV split for initial data
    train_indices = []
    test_indices = []

    if args.stratification_strategy == 'groups_stratify':
        train['groups'] = (
            train['accepted_function'] + 
            train['rejected_function'] + 
            train['accepted_product'] + 
            train['rejected_product']
        )
        kf = GroupKFold(n_splits=5)

        kf.get_n_splits(train, train['target'], train['groups'])

        for train_index, test_index in kf.split(train, train['target'],train['groups']):
            train_indices.append(train_index)
            test_indices.append(test_index)

        data_sufix = 'cv_group'

    elif args.stratification_strategy == 'target_stratify':
        kf = StratifiedKFold(n_splits=5)

        kf.get_n_splits(train, train['target'])

        for train_index, test_index in kf.split(train, train['target']):
            train_indices.append(train_index)
            test_indices.append(test_index)

        data_sufix = 'cv'

    else:
        raise ValueError('Unrecognized stratification strategy')

    train_indices = np.array(train_indices)
    test_indices = np.array(test_indices)

     # Create CV split for `exploaded` data
    train = train.reset_index()
    train = train.explode('text').reset_index(drop=True)

    exploaded_train_indices = []
    exploaded_test_indices = []

    for i in range(len(train_indices)):
        exploaded_train_indices.append(train[train['index'].isin(train_indices[i])].index.values)
        exploaded_test_indices.append(train[train['index'].isin(test_indices[i])].index.values)


    np.save(
        path.join(path.dirname(args.df_path), 'train_'+data_sufix+'_indices.npy'), 
        train_indices
    )
    np.save(
        path.join(path.dirname(args.df_path), 'test_'+data_sufix+'_indices.npy'), 
        test_indices
    )
    np.save(
        path.join(path.dirname(args.df_path), 'exploded_train_'+data_sufix+'_indices.npy'), 
        exploaded_train_indices
    )
    np.save(
        path.join(path.dirname(args.df_path), 'exploded_test_'+data_sufix+'_indices.npy'), 
        exploaded_test_indices
    )

    print('Completed')


