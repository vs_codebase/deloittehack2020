# DeloitteHack2020

Codebase of PowerRangers team for Deloitte Hackaton 2020

# Bert
![Bert](berts.jpeg "Bert")

# Pipeline
1.  Download dataset with `kaggle competitions download -c company-acceptance-prediction`
2.  `python preprocessing.py --df_path /your/fancy/path/train_df.pkl --model_name bert-base-uncased --df_name train --extract_target --extract_atten_mask --extract_segment_id --heavy_clean` to extract train data and target
3.  `python preprocessing.py --df_path /your/fancy/path/test_df.pkl --model_name bert-base-uncased --df_name test --extract_atten_mask --extract_segment_id --heavy_clean` to extract test data
4.  `python create_cv_split.py --df_path /your/fancy/path/train_df.pkl --stratification_strategy groups_stratify` to create cv stratification
4.  `CUDA_VISIBLE_DEVICES=0 python train.py -c train_configs/dima_bert_deloitte_scheduler_lr_heavypp_start_groupval.json --exp-name dimabert_deloitte` to run your model to train
5.  `CUDA_VISIBLE_DEVICES=1 python inference.py --config inference_configs/dima_bert_deloitte_scheduler_lr_heavypp_start_groupval_inference.json` to run inference on val and test data
6.  Use `blend.ipynb` to create final submission


# Boosted Pipeline

Of course, we are not interested in one Bert. We are interested in many Berts.
So we can use 2 different slicing stratagies (`start` and `use_had_tail`) and train 5 folds on each dataset.
Then we can use each model to predict on both datasets and get overall 5*2*2 = 20 predictions.
And then BLEEEEEEEEND!!!

* `python preprocessing.py --df_path /your/fancy/path/train_df.pkl --model_name bert-base-uncased --df_name train --extract_atten_mask --extract_segment_id --heavy_clean --slice_strategy use_had_tail` to extract new train data 
* `python preprocessing.py --df_path /your/fancy/path/test_df.pkl --model_name bert-base-uncased --df_name test --extract_atten_mask --extract_segment_id --heavy_clean --slice_strategy use_had_tail` to extract new test data 
* change pathes in train and inference configs to train and predict on different datesets

# Not only BERT!!!

* LSTM approach (explore `LSTM`)
* Pseudo-labeling (explore `Pseudo.ipynb`)

# P.S
If you are interseted in our submissions and blending and their results on validation, you can explore `probs.zip`
If you have any questions, suggestions or you have found some bugs or mistackes you can contact me with telegram (@@Vova_Sydor)