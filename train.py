from __future__ import print_function
import builtins as __builtin__

import os
import argparse
import json

from os import path
from shutil import copyfile
from glob import glob

import pandas as pd
import numpy as np
import torch
import torch.nn as nn

from tensorboardX import SummaryWriter
from transformers import BertModel, get_linear_schedule_with_warmup, BertTokenizer

from trainer import TrainEpoch, ValidEpoch
from losses import CrossEntropyOnSteroids
from metrics import AccuracyMetric
from model import VovaWrapper, DimaBert, VitalyiBert
from loader import BertDataset
from optimizers import get_dima_optimizer
from telegram_loging import TelegramCallback
from augmentations import RandomTokenPerm

def print(str_to_print, telegram_bot=None):
    if telegram_bot is not None:
        telegram_bot.send_message(str_to_print)
        
    return __builtin__.print(str_to_print)

def save_model(s_model, s_optimizer, s_scheduler, 
               s_iteration_train, 
               s_logs_train, s_logs_val,
               path):
    torch.save({
        'model':s_model.state_dict(),
        'optimizer':s_optimizer.state_dict(),
        'scheduler':s_scheduler.state_dict(),
        'iteration':{
            'train':s_iteration_train,
        },
        'logs':{
            'train':s_logs_train,
            'val':s_logs_val
        }
    }, path)
    
    
def load_checkpoint(checkpoint_path, model, optimizer=None, scheduler=None):
    assert os.path.isfile(checkpoint_path)
    
    checkpoint_dict = torch.load(checkpoint_path, map_location='cpu')

    if 'iteration' in checkpoint_dict:
        iteration_train = checkpoint_dict['iteration']['train']
    else:
        iteration_train = 0
    
    if 'optimizer' in checkpoint_dict and optimizer is not None:
        optimizer.load_state_dict(checkpoint_dict['optimizer'])
        
    if 'scheduler' in checkpoint_dict and scheduler is not None:
        scheduler.load_state_dict(checkpoint_dict['scheduler'])

    
    model.load_state_dict(checkpoint_dict['model'])
    print("Loaded checkpoint '{}' (iteration {})" .format(
          checkpoint_path, iteration_train))
    
    return model, optimizer, scheduler, iteration_train
       

def train_model(chckp_path, summary_path, checkpoint_path, # logging pathes
                ids_path, mask_path, segment_path, target_path, # data pathes
                pseudo_ids_path, pseudo_mask_path, pseudo_segment_path, pseudo_target_path,
                device, 
                batch_size,
                n_epochs, early_stop_rounds, # overfitting reduction 
                bert_model_to_use,
                learning_rate_base, learning_rate_clf, # learning rates
                warmup,
                telegram_config, use_tg_logging, # telegram
                scalar_freq_logging, # logging freqancies
                cv_train_idx_path, cv_test_idx_path, fold_number, # cv config
                stat_to_monitor, two_gpus, grad_acumulation,
                num_labels
                ):
    
    # Check if stat_to_monitor is valid
    assert stat_to_monitor in ['loss', 'metric'], 'We can monitor only loss'
    
    # Telegram Bot
    telegram_bot = TelegramCallback(telegram_config) if use_tg_logging else None
    
    # Get cv split
    cv_train_idx= np.load(cv_train_idx_path, allow_pickle=True)[fold_number]
    cv_test_idx= np.load(cv_test_idx_path, allow_pickle=True)[fold_number]
    print("Fold: {}".format(fold_number), telegram_bot)

    # Prepare arrays
    ids = torch.load(ids_path)
    masks = torch.load(mask_path)
    segments = torch.load(segment_path)
    target = torch.load(target_path)

    if pseudo_ids_path != "":
        pseudo_ids = torch.load(pseudo_ids_path)
        pseudo_masks = torch.load(pseudo_mask_path)
        pseudo_segments = torch.load(pseudo_segment_path)
        pseudo_target = torch.load(pseudo_target_path)

    if pseudo_ids_path != "":
        print("Train len : {}".format(len(cv_train_idx) + len(pseudo_ids)), telegram_bot)
    else:
        print("Train len : {}".format(len(cv_train_idx)), telegram_bot)

    print("Test len : {}".format(len(cv_test_idx)), telegram_bot)
    
    if pseudo_ids_path != "":
         train_dataset = train_dataset = BertDataset(
            ids=torch.cat([ids[cv_train_idx],pseudo_ids],dim=0),
            masks=torch.cat([masks[cv_train_idx],pseudo_masks],dim=0),
            segments=torch.cat([segments[cv_train_idx],pseudo_segments],dim=0),
            y=torch.cat([target[cv_train_idx],pseudo_target.long()],dim=0)
            )
    else:
        train_dataset = train_dataset = BertDataset(
            ids=ids[cv_train_idx],
            masks=masks[cv_train_idx],
            segments=segments[cv_train_idx],
            y=target[cv_train_idx]
            )
    
    val_dataset = BertDataset(
        ids=ids[cv_test_idx],
        masks=masks[cv_test_idx],
        segments=segments[cv_test_idx],
        y=target[cv_test_idx]
        )
    
    
    # Create dataloaders
    train_dataloader = torch.utils.data.DataLoader(
        train_dataset, 
        batch_size=batch_size, 
        shuffle=True,
        drop_last=True,
        num_workers=os.cpu_count()//2)
    val_dataloader = torch.utils.data.DataLoader(
            val_dataset, 
            batch_size=batch_size, 
            shuffle=False,
            drop_last=True,
            num_workers=os.cpu_count()//2)
    
    
    
    # Get model
    model = DimaBert.from_pretrained(bert_model_to_use, num_labels=num_labels).to(device)

    if two_gpus:
        model = nn.DataParallel(model)
        
    print(model)
    
    # Get optimizer 
    optimizer = get_dima_optimizer(
        dima_model=model,
        clf_lr=learning_rate_clf,
        base_lr=learning_rate_base
    )
    
    scheduler = get_linear_schedule_with_warmup(
            optimizer,
            num_warmup_steps=warmup,
            num_training_steps=(
                n_epochs * len(train_dataloader) / grad_acumulation
            ),
        )
    # Load checkpoint
    if checkpoint_path != "":
        model, optimizer, scheduler, train_iteration = load_checkpoint(checkpoint_path,
                                                                       model,
                                                                       optimizer,
                                                                       scheduler)
    else:
        train_iteration = 0
        
    start_epoch = max(0, int(train_iteration / len(train_dataloader)))
                                 
    # Summary writer
    writer = SummaryWriter(summary_path)
    
    # Loss functions and metrics
    loss_f_train = CrossEntropyOnSteroids()  
    metric_f_train = AccuracyMetric()

    loss_f_test = CrossEntropyOnSteroids()
    metric_f_test = AccuracyMetric()
    
    
    train_epoch = TrainEpoch(
    model, 
    loss=loss_f_train, 
    metrics=metric_f_train, 
    optimizer=optimizer,
    scheduler=scheduler,
    device=device,
    verbose=True,
    writer=writer,
    scalar_freq_logging=scalar_freq_logging,
    gradient_accumulation=grad_acumulation
    )

    val_epoch = ValidEpoch(
    model, 
    loss=loss_f_test, 
    metrics=metric_f_test, 
    device=device,
    verbose=True,
    writer=writer,
    scalar_freq_logging=scalar_freq_logging,
    )
      
    # Training Loop 
    if stat_to_monitor == 'loss':
        best_metric = 100
    elif stat_to_monitor == 'metric':
        best_metric = 0
        
    early_stop_acum = 0

    for epoch in range(start_epoch, n_epochs):

        print('Epoch {}'.format(epoch), telegram_bot)

        train_logs, train_iteration = train_epoch.run(train_dataloader, train_iteration)
        val_logs, _ = val_epoch.run(val_dataloader, train_iteration)
        
        if stat_to_monitor == 'loss':
            
            if val_logs['CrossEntropy'] < best_metric:
                print('Improved', telegram_bot)

                best_metric = val_logs['CrossEntropy']
                early_stop_acum = 0
                save_model(model, optimizer, scheduler, 
                           train_iteration,
                           train_logs, val_logs,
                           path.join(chckp_path,'best_model.pt'))
            else:
                early_stop_acum += 1
                
        elif stat_to_monitor == 'metric':
            
            if val_logs['metric'] > best_metric:
                print('Improved', telegram_bot)

                best_metric = val_logs['metric']
                early_stop_acum = 0
                save_model(model, optimizer, scheduler, 
                           train_iteration,
                           train_logs, val_logs,
                           path.join(chckp_path,'best_model.pt'))
            else:
                early_stop_acum += 1
                
            
        print('Early stopping rounds :{}'.format(early_stop_acum))

        save_model(model, optimizer, scheduler, 
                       train_iteration,
                       train_logs, val_logs,
                       path.join(chckp_path,'model_{}.pt'.format(epoch)))

        print('Train Logs :', telegram_bot)
        print('\n'.join([k+' : {0:.5f}'.format(v) for k,v in train_logs.items()]), telegram_bot)
        print('Val Logs :', telegram_bot)
        print('\n'.join([k+' : {0:.5f}'.format(v) for k,v in val_logs.items()]), telegram_bot)
        
        print('Base LR :{}\nCLF LR: {}'.format(optimizer.param_groups[0]['lr'], optimizer.param_groups[1]['lr']))

        if early_stop_acum == early_stop_rounds:
            print('Early stopped on epoch: {}'.format(epoch), telegram_bot)
            break

    return best_metric
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str,
                        help='JSON file for configuration')
    parser.add_argument('--exp-name', type=str,
                        help='name of the experiment')
    
    args = parser.parse_args()

    # Parse configs
    with open(args.config) as f:
        data = f.read()
    config = json.loads(data)
    
    # Run Training over Folds
    if isinstance(config['fold_number'],list):
        folds_to_run = config['fold_number']
        best_metrics = []
        
        for f in folds_to_run:
            config['fold_number'] = f
            
            config['chckp_path'] = path.join('checkpoints', args.exp_name)+'_fold{}'.format(f)
            config['summary_path'] = path.join('summary', args.exp_name)+'_fold{}'.format(f)
            source_code_path = path.join(config['summary_path'],'source_code')

            # Prepare Summary and Checkpoint dirs
            os.makedirs(config['chckp_path'], exist_ok=True)
            os.makedirs(config['summary_path'], exist_ok=True)
            os.makedirs(source_code_path, exist_ok=True)

            # Dump config
            with open(path.join(config['summary_path'], 'config.json'), 'w') as fp:
                json.dump(config, fp)

            # Dump source code
            for f in glob("*.py"):
                copyfile(f, path.join(source_code_path, f))

            print(config)

            best_metric = train_model(**config) 
            best_metrics.append(best_metric)
            with open(path.join(config['summary_path'],"results.txt"), "w") as file:
                file.write('Val metric: {}'.format(best_metric))

        with open(path.join(config['summary_path'],"cv_results.txt"), "w") as file:
                file.write('Val metric: {}'.format(np.mean(best_metrics)))
      
    else:
        config['chckp_path'] = path.join('checkpoints', args.exp_name)
        config['summary_path'] = path.join('summary', args.exp_name)
        source_code_path = path.join(config['summary_path'],'source_code')

        # Prepare Summary and Checkpoint dirs
        os.makedirs(config['chckp_path'], exist_ok=True)
        os.makedirs(config['summary_path'], exist_ok=True)
        os.makedirs(source_code_path, exist_ok=True)

        # Dump config
        with open(path.join(config['summary_path'], 'config.json'), 'w') as fp:
            json.dump(config, fp)

        # Dump source code
            for f in glob("*.py"):
                copyfile(f, path.join(source_code_path, f))

        print(config)

        best_metric = train_model(**config)
        with open(path.join(config['summary_path'],"results.txt"), "w") as file:
                file.write('Val metric: {}'.format(best_metric))

